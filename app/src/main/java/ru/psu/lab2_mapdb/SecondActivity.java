package ru.psu.lab2_mapdb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ru.psu.lab2_mapdb.Database.AppDatabase;
import ru.psu.lab2_mapdb.Database.DBMarker;
import ru.psu.lab2_mapdb.Database.DBPhoto;
import ru.psu.lab2_mapdb.Database.DatabaseCreator;

public class SecondActivity extends Activity {
    private Photo photo;
    MyTask mt;
    private ArrayList<String> imagePaths;

    private GridView gridView;
    private GridViewAdapter gridAdapter;
    private static Integer curMarker;
    AppDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        db = DatabaseCreator.getDB(getApplicationContext());

        if (savedInstanceState != null) {
            curMarker = savedInstanceState.getInt("marker");
            imagePaths = savedInstanceState.getStringArrayList("imagePaths");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Photo.verifyStoragePermissions(this);
        gridView = (GridView) findViewById(R.id.gridView);
        getFromFirstActivity();




    }

    private void getFromFirstActivity() {
        Intent intent = getIntent();
        curMarker = intent.getIntExtra("marker", -1);

        new AsyncTask <Void, Void,List<String>>() {
            @Override
            protected List<String> doInBackground(Void... voids) {
                List <String> list = db.photoDao().getPaths(curMarker);
                imagePaths = new ArrayList<String>(list);
                return null;
            } /*Your code(e.g. doInBackground )*/

            @Override
            protected void onPostExecute(List<String> result) {
                super.onPostExecute(result);
                updateGridView();
            }
           }.execute();


    }


    private void updateGridView() {
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);
    }

    private ArrayList<ImageItem> getData() {

        final ArrayList<ImageItem> imageItems = new ArrayList<>();

        for (int i = 0; i < imagePaths.size(); i++) {
            Bitmap bitmap = getBitmap(imagePaths.get(i));
            imageItems.add(new ImageItem(bitmap, "Image#" + i));
        }
        return imageItems;
    }

    private Bitmap getBitmap(String pathName) {
        BitmapFactory.Options options;
        options = new BitmapFactory.Options();
        try {
            options.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeFile(pathName, options);
            return bitmap;
        } catch (OutOfMemoryError e) {
            options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(pathName, options);
            return bitmap;
        }
    }

    public void onClickPhoto(View view) throws InterruptedException, ExecutionException, TimeoutException {
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getData());
        mt = new MyTask();
        mt.execute(imagePaths);

        if (mt == null)
            return;
        boolean result = mt.get(1, TimeUnit.SECONDS);
        if (result)
            gridView.setAdapter(gridAdapter);
    }

    private void dispatchTakePictureIntent() {
        photo = new Photo();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (photo.getPhotoFile() == null) {
                try {
                    photo.setPhotoFile(photo.createImageFile());
                    imagePaths.add(photo.getPhotoFile().getAbsolutePath().toString());
                } catch (IOException ex) {
                    Toast.makeText(this, R.string.file_error, Toast.LENGTH_SHORT).show();
                }
            }
            if (photo.getPhotoFile() != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo.getPhotoFile()));
                startActivityForResult(takePictureIntent, 1);
            }
        }

        new AsyncTask <Integer, Void,Void>() {
            @Override
            protected Void doInBackground(Integer... markers) {
                //for (String path : imagePaths)
               // {
                    DBPhoto ph = new DBPhoto(photo.getPhotoFile().getAbsolutePath().toString(),markers[0]);
                    db.photoDao().insertAll(ph);
               // }
                return null;
            } /*Your code(e.g. doInBackground )*/

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

            }}.execute(curMarker);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        updateGridView();
    }

    public void onClickBack(View view) {

        Intent intent = new Intent();

        intent.putExtra("marker", curMarker);
        setResult(RESULT_OK, intent);
        finish();
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("imagePaths" , imagePaths);
        outState.putInt("marker" , curMarker);

    }

    /**1) Тип входных данных. Это данные, которые пойдут на вход AsyncTask
     2) Тип промежуточных данных. Данные, которые используются для вывода промежуточных результатов
     3) Тип возвращаемых данных. То, что вернет AsyncTask после работы.*/
    class MyTask extends AsyncTask <ArrayList<String>, Void, Boolean> {

        public MyTask()
        {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Boolean doInBackground(ArrayList<String>... params) {
            dispatchTakePictureIntent();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            // gridView.setAdapter(gridAdapter);
        }
    }
}
