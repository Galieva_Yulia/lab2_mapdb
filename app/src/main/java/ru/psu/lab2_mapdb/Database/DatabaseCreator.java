package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseCreator {

    private static AppDatabase db;
    private static final Object LOCK = new Object();

    public synchronized static AppDatabase getDB(Context context){
        if(db == null) {
            synchronized (LOCK) {
                if (db == null) {
                    db = Room.databaseBuilder(context,
                            AppDatabase.class, "database").build();
                }
            }
        }
        return db;
    }
}