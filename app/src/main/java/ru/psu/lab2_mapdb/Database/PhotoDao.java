package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 03.03.2018.
 */
@Dao
public interface PhotoDao {
    @Query("select * from DBPhoto")
    List<DBPhoto> getall();

    @Query("select * from DBPhoto where photoId = :id")
    DBPhoto getById(String id);

    @Query("select path from DBPhoto where ownerId = :id")
    List<String> getPaths(Integer id);

    @Query("select markerId from DBMarker where markerId==:id")
    Integer findMarkerId(Integer id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(DBPhoto... photos);

    @Insert
    void insertOne(DBPhoto photo);

    @Update
    public void updeteUsers(DBPhoto... photos);

    @Update
    void updateOne(DBPhoto photo);

    @Delete
    void deleteOne(DBPhoto photo);

}
