package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import static android.arch.persistence.room.ForeignKey.CASCADE;

/*@Entity(foreignKeys = @ForeignKey(entity = DBMarker.class,
        parentColumns = "markerId",childColumns = "ownerId", onDelete = CASCADE))*/
@Entity
public class DBPhoto  {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int photoId;

    @ColumnInfo(name = "path")
    public String path;

    @ColumnInfo(name = "ownerId")
    public  int ownerId; // this ID points to a DBMarker

    public DBPhoto(String path, int ownerId) {
        this.path = path;
        this.ownerId = ownerId;
    }
}