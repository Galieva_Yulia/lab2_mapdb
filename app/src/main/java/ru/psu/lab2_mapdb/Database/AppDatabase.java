package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {DBMarker.class, DBPhoto.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MarkerDao markerDao();
    public abstract PhotoDao photoDao();
}
