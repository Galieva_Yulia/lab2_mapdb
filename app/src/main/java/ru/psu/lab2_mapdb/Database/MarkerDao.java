package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MarkerDao
{
    @Query("select * from DBMarker")
    List<DBMarker> getall();

    @Query("select * from DBMarker where markerId = :id")
    DBMarker getById(String id);

    @Query("select markerId from DBMarker where lat == :lat and lng == :lng")
    Integer getMarkerId(double lat, double lng);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(DBMarker... markers);

    @Insert
    void insert(DBMarker marker);

    @Update
    void updateUsers(DBMarker... markers);

    @Update
    void update(DBMarker marker);

    @Delete
    void delete(DBMarker marker);

}
