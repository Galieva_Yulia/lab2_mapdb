package ru.psu.lab2_mapdb.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class DBMarker  {
    @PrimaryKey (autoGenerate = true)
    @NonNull
    public int markerId;

    @ColumnInfo(name = "lat") //аннотация
    public double lat;

    @ColumnInfo(name = "lng")
    public double lng;


    public DBMarker( double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
