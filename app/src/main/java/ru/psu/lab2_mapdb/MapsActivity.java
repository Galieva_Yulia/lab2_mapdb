package ru.psu.lab2_mapdb;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import ru.psu.lab2_mapdb.Database.AppDatabase;
import ru.psu.lab2_mapdb.Database.DBMarker;
import ru.psu.lab2_mapdb.Database.DatabaseCreator;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;
    private static int curMarkerId;
    private ArrayList <DBMarker> markers = new ArrayList<>();
    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       db = DatabaseCreator.getDB(getApplicationContext());

        new AsyncTask <Void, Void,Void>() {
            List<DBMarker> list;
            @Override
            protected Void doInBackground(Void... voids) {
                list = db.markerDao().getall();
                return null;
            } /*Your code(e.g. doInBackground )*/

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                for (DBMarker m : list)
                {
                    markers.add(m);
                }
            }}.execute();


        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();

        if (markers.size()!= 0) {
            for (DBMarker m : markers) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(m.lat,m.lng))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
            }
        }

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng arg0) {

                mMap.addMarker(new MarkerOptions()
                        .position(arg0)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

                final DBMarker marker = new DBMarker(arg0.latitude, arg0.longitude);
                markers.add(marker);
              //  curMarkerId = marker;

                new AsyncTask <Void, Void, List<DBMarker>>() {
                    @Override
                    protected List<DBMarker> doInBackground(Void... voids) {
                        DBMarker m = new DBMarker(marker.lat, marker.lng);
                        db.markerDao().insert(m);
                        return null;
                    } /*Your code(e.g. doInBackground )*/ }.execute();

            }
        });
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putParcelableArrayList("markers" , markers);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //curMarkerId = new DBMarker(marker.getPosition().latitude,marker.getPosition().longitude);
 final DBMarker m = new DBMarker(marker.getPosition().latitude,marker.getPosition().longitude);
        new AsyncTask <Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... voids) {
                curMarkerId = db.markerDao().getMarkerId(m.lat, m.lng);
                return null;
            } /*Your code(e.g. doInBackground )*/  @Override
            protected void onPostExecute(Integer result) {
                super.onPostExecute(result);
                Intent intent = new Intent(MapsActivity.this, SecondActivity.class);
                intent.putExtra("marker", curMarkerId);
                startActivityForResult(intent, 1);
            }}.execute();


        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null) {return;}

        curMarkerId = data.getIntExtra("marker", -1);
    }

    private void setUpMap() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            moveToMyLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},99);
        }
        mMap.setOnMarkerClickListener((GoogleMap.OnMarkerClickListener) this);
    }

    private void moveToMyLocation() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation("network");
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
    }
}

